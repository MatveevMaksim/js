function clearButton(){
	document.getElementById("x1").value = '';
	document.getElementById("x2").value = '';
}

function buttonClick(){
	var x1 = parseInt(document.getElementById('x1').value);
	var x2 = parseInt(document.getElementById('x2').value);
	var radio = document.getElementsByName('calcChoice');
	var resultDiv = document.getElementById('result');

	for (var i=0; i<radio.length; i++) {
		if (radio[0].checked){
			if (!isNaN(parseFloat(x1,x2)) && isFinite(x1,x2)){
				var summ=0;
				for (var i = x1; i <= x2; i++)
					resultDiv.innerHTML = ("Сумма всех чисел от x1 до x2 равна " +(summ += i));
			} 
			else {
				alert ("Поля x1 и x2 должны быть заполнены!");
			}
		}
		else {
			if (!isNaN(parseFloat(x1,x2)) && isFinite(x1,x2)){
				var compos=1;
				for (var i = x1; i <= x2; i++)
					resultDiv.innerHTML = ("Произведение всех чисел от x1 до x2 равна " +(compos *= i));
			}
			else {
				alert ("Поля x1 и x2 должны быть заполнены!");
			}
		}
	}
	
}